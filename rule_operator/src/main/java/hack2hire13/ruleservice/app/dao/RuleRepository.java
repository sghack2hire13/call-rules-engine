package hack2hire13.ruleservice.app.dao;

import hack2hire13.ruleservice.app.domain.Rule;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends PagingAndSortingRepository<Rule, Long> {
}
