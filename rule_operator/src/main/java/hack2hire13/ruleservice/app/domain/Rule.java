package hack2hire13.ruleservice.app.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = "unique_rule_definition",
        columnNames = {"eventType", "operator", "parameter", "aggregationType"})
})
public class Rule {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private EventType eventType;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private Operator operator;

  @Column(nullable = false)
  private Double parameter;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private AggregationType aggregationType;

  @Column(nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastModifiedTime;

  @Column(nullable = false)
  private String promotion;

  @Column
  private String description;

  public Rule() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public Operator getOperator() {
    return operator;
  }

  public void setOperator(Operator operator) {
    this.operator = operator;
  }

  public Double getParameter() {
    return parameter;
  }

  public void setParameter(Double parameter) {
    this.parameter = parameter;
  }

  public AggregationType getAggregationType() {
    return aggregationType;
  }

  public void setAggregationType(AggregationType aggregationType) {
    this.aggregationType = aggregationType;
  }

  public Date getLastModifiedTime() {
    return lastModifiedTime;
  }

  public void setLastModifiedTime(Date lastModifiedTime) {
    this.lastModifiedTime = lastModifiedTime;
  }

  public String getPromotion() {
    return promotion;
  }

  public void setPromotion(String promotion) {
    this.promotion = promotion;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
