package hack2hire13.ruleservice.app.domain;

public enum AggregationType {
  DAILY,
  WEEKLY,
  MONTHLY,
  YEARLY
}
