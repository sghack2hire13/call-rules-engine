package hack2hire13.ruleservice.app.domain;

public enum Operator {
  EQUALS,
  GREATER,
  GREATER_OR_EQUALS,
  LESS,
  LESS_OR_EQUALS;
}
