package hack2hire13.ruleservice.app.domain;

public enum EventType {
  ALL_CALLS_DURATION,
  NO_OF_CALLS,
  ABNORMALLY_DROPPED_CALLS,
  UNSUCCESSFUL_ATTEMPTS
}
