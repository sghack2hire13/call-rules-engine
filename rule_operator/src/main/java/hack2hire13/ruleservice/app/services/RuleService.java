package hack2hire13.ruleservice.app.services;

import hack2hire13.ruleservice.app.domain.Rule;
import hack2hire13.ruleservice.app.dao.RuleRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

/**
 * @author Andrei Safronov
 */
@Service
public class RuleService {

  @Autowired
  private RuleRepository repository;

  @Transactional
  public Rule persistRule(Rule rule) {
    rule.setLastModifiedTime(new Date());
    return repository.save(rule);
  }

  @Transactional
  public Rule updateRule(Rule rule) {
    rule.setLastModifiedTime(new Date());
    repository.save(rule);
    return rule;
  }

  @Transactional
  public void deleteRule(Rule rule) {
    repository.delete(rule);
  }

  @Transactional
  public List<Rule> findAll() {
    PageRequest request = new PageRequest(0, Integer.MAX_VALUE, Direction.ASC, "id");
    return repository.findAll(request).getContent();
  }
}
