package hack2hire13.ruleservice.app.controller;

/**
 * @author Andrei Safronov
 */
public class RuleException extends RuntimeException {

  public RuleException(String message) {
    super(message);
  }

}
