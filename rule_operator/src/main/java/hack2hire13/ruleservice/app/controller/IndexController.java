package hack2hire13.ruleservice.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Andrei Safronov
 */
@Controller
public class IndexController {

  @RequestMapping(path = "/", method = RequestMethod.GET)
  public String goHome() {
    return "index";
  }

}
