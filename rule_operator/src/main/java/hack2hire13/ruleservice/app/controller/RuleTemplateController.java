package hack2hire13.ruleservice.app.controller;

import hack2hire13.ruleservice.app.domain.Rule;
import hack2hire13.ruleservice.app.services.RuleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
public class RuleTemplateController {

  @Autowired
  private RuleService service;

  @RequestMapping(value = "/rules", method = RequestMethod.GET)
  public List<Rule> findAll() {
    return service.findAll();
  }

  @RequestMapping(value = "/rules", method = RequestMethod.POST)
  public Rule create(@RequestBody Rule rule) {
    if (rule.getId() != null) {
      rule.setId(null);
    }
    try {
      return service.persistRule(rule);
    } catch (Exception e) {
      throw new RuleException(e.getMessage());
    }
  }

  @RequestMapping(value = "/rules", method = RequestMethod.PUT)
  public Rule update(@RequestBody Rule rule) {
    if (rule.getId() == null) {
      throw new RuleException("Id is missing");
    }
    try {
      return service.updateRule(rule);
    } catch (Exception e) {
      throw new RuleException(e.getMessage());
    }
  }

  @RequestMapping(value = "/rules", method = RequestMethod.DELETE)
  public void delete(@RequestBody Rule rule) {
    if (rule.getId() == null) {
      throw new RuleException("Id is missing");
    }
    try {
      service.deleteRule(rule);
    } catch (Exception e) {
      throw new RuleException(e.getMessage());
    }
  }

  @ExceptionHandler(RuleException.class)
  public ResponseEntity<Object> handleErrors(RuleException exception, WebRequest request) {
    // for simplicity handle everything as bad request
    return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
  }

}
