import csv
import boto3
from itertools import islice
import json
import random
import time

kinesis = boto3.client('kinesis',region_name='ap-southeast-1')

if __name__ == '__main__':
    cdr_file = 'h5000.txt'
    #cdr_file = 'cdr1.txt'

    fields = ("cdrType", "imei", "imsi", "callingNumber", "calledNumber", "recordingEntity", "location", "callReference", "callDuration", "answerTime", "seizureTime", "releaseTime", "causeForTermination", "basicService", "mscAddress")
    with open(cdr_file, 'r') as csvfile:
        cdr_reader = csv.DictReader(csvfile, fields)
        for n_rows in iter(lambda: tuple(islice(cdr_reader, random.randint(10,25))),()):
            print len(n_rows)
            data_list = []
            for v in n_rows:
                data_list.append({
                    'Data': json.dumps(v),
                    'PartitionKey': 'data'
                })
            #print data_list
            kinesis.put_records(
                        StreamName='cdr-stream',
                        Records=data_list
                        )
            time.sleep(random.randint(1,8))
