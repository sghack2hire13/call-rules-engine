from __future__ import print_function
import os
import base64
from datetime import datetime
import json
import hashlib
from elasticsearch import Elasticsearch
from elasticsearch import helpers

print('Loading function')

es = Elasticsearch([{"host":"search-es-warehouse-eu43mnvx6nq2uteqqne4iooyhy.ap-southeast-1.es.amazonaws.com", "port":80}])


def send_to_es(data, es):
    actions = []
    es_index = os.environ.get('ES_INDEX', 'test9')
    for v in data:
        h = json.dumps(v)
        v.update({"@timestamp": datetime.utcnow()})
        d = {
            "_index": es_index,
            "_type": "cdr",
            "_source": v,
            "_id": hashlib.sha256(h).hexdigest()
        }
        actions.append(d)
    helpers.bulk(es, actions, chunk_size=100)

def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    data = []
    for record in event['Records']:
        # Kinesis data is base64 encoded so decode here
        payload = base64.b64decode(record['kinesis']['data'])
        print("Decoded payload: " + payload)
        data.append(json.loads(payload))
    send_to_es(data, es)
    return 'Successfully processed {} records.'.format(len(event['Records']))
