package hack2hire13.stream.app.domain;

/**
 * @author Andrei Safronov
 */
public enum CDRType {

  MOBILE_ORIGINATED("MOC"),
  MOBILE_TERMINATED("MTC");

  private final String code;

  CDRType(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public static CDRType getByCode(String code) {
    for (CDRType type : CDRType.values()) {
      if (type.getCode().equals(code))
        return type;
    }
    return null;
  }

}
