package hack2hire13.stream.app.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author Andrei Safronov
 */
@Entity
@Table(indexes = {
    @Index(name = "cdrType_index", columnList = "cdrType"),
    @Index(name = "callingNumber_index", columnList = "callingNumber"),
    @Index(name = "causeForTermination_index", columnList = "causeForTermination"),
    @Index(name = "day_index", columnList = "day"),
    @Index(name = "week_index", columnList = "week"),
    @Index(name = "month_index", columnList = "month"),
    @Index(name = "year_index", columnList = "year"),
})
public class CallRecordEntity {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private CDRType cdrType;

  @Column(nullable = false)
  private String callingNumber;

  @Column(nullable = false)
  private String calledNumber;

  @Column(nullable = false)
  private String location;

  @Column(nullable = false)
  private Double callDuration;

  @Column(nullable = false)
  @Enumerated(EnumType.ORDINAL)
  private CauseForTermination causeForTermination;

  @Column(nullable = false)
  private int day;

  @Column(nullable = false)
  private int week;

  @Column(nullable = false)
  private int month;

  @Column(nullable = false)
  private int year;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public CDRType getCdrType() {
    return cdrType;
  }

  public void setCdrType(CDRType cdrType) {
    this.cdrType = cdrType;
  }

  public String getCallingNumber() {
    return callingNumber;
  }

  public void setCallingNumber(String callingNumber) {
    this.callingNumber = callingNumber;
  }

  public String getCalledNumber() {
    return calledNumber;
  }

  public void setCalledNumber(String calledNumber) {
    this.calledNumber = calledNumber;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Double getCallDuration() {
    return callDuration;
  }

  public void setCallDuration(Double callDuration) {
    this.callDuration = callDuration;
  }

  public CauseForTermination getCauseForTermination() {
    return causeForTermination;
  }

  public void setCauseForTermination(CauseForTermination causeForTermination) {
    this.causeForTermination = causeForTermination;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public int getWeek() {
    return week;
  }

  public void setWeek(int week) {
    this.week = week;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
