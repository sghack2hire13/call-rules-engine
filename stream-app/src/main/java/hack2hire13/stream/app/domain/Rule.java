package hack2hire13.stream.app.domain;

import java.util.Date;

/**
 * @author Andrei Safronov
 */
public class Rule {

  private Long id;
  private EventType eventType;
  private Operator operator;
  private Double parameter;
  private AggregationType aggregationType;
  private Date lastModifiedTime;
  private String promotion;
  private String description;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public Operator getOperator() {
    return operator;
  }

  public void setOperator(Operator operator) {
    this.operator = operator;
  }

  public Double getParameter() {
    return parameter;
  }

  public void setParameter(Double parameter) {
    this.parameter = parameter;
  }

  public AggregationType getAggregationType() {
    return aggregationType;
  }

  public void setAggregationType(AggregationType aggregationType) {
    this.aggregationType = aggregationType;
  }

  public Date getLastModifiedTime() {
    return lastModifiedTime;
  }

  public void setLastModifiedTime(Date lastModifiedTime) {
    this.lastModifiedTime = lastModifiedTime;
  }

  public String getPromotion() {
    return promotion;
  }

  public void setPromotion(String promotion) {
    this.promotion = promotion;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Rule rule = (Rule) o;

    if (!id.equals(rule.id)) {
      return false;
    }
    if (eventType != rule.eventType) {
      return false;
    }
    if (operator != rule.operator) {
      return false;
    }
    if (!parameter.equals(rule.parameter)) {
      return false;
    }
    if (aggregationType != rule.aggregationType) {
      return false;
    }
    return promotion.equals(rule.promotion);
  }

  @Override
  public int hashCode() {
    int result = id.hashCode();
    result = 31 * result + eventType.hashCode();
    result = 31 * result + operator.hashCode();
    result = 31 * result + parameter.hashCode();
    result = 31 * result + aggregationType.hashCode();
    result = 31 * result + promotion.hashCode();
    return result;
  }
}
