package hack2hire13.stream.app.domain;

/**
 * @author Andrei Safronov
 */
public enum Operator {
  EQUALS,
  GREATER,
  GREATER_OR_EQUALS,
  LESS,
  LESS_OR_EQUALS;
}
