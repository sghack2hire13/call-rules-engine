package hack2hire13.stream.app.domain;

/**
 * @author Andrei Safronov
 */
public enum AggregationType {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
