package hack2hire13.stream.app.domain;

/**
 * @author Andrei Safronov
 */
public enum CauseForTermination {

  NORMAL_CALL("00"),
  ABNORMALLY_DROPPED("04"),
  UNSUCCESSFUL_ATTEMPT("03");

  private final String code;

  CauseForTermination(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public static CauseForTermination getByCode(String code) {
    for (CauseForTermination cause : CauseForTermination.values()) {
      if (cause.getCode().equals(code))
        return cause;
    }
    return null;
  }
}
