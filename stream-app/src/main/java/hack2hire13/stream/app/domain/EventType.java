package hack2hire13.stream.app.domain;

/**
 * @author Andrei Safronov
 */
public enum EventType {

  ALL_CALLS_DURATION,
  NO_OF_CALLS,
  ABNORMALLY_DROPPED_CALLS,
  UNSUCCESSFUL_ATTEMPTS

}
