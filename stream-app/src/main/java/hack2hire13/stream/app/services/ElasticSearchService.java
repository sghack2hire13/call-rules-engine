package hack2hire13.stream.app.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import hack2hire13.stream.app.domain.CallRecordEntity;
import hack2hire13.stream.app.domain.Rule;
import hack2hire13.stream.app.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Andrei Safronov
 */
@Service
public class ElasticSearchService {

  private static final Logger log = LoggerFactory.getLogger(ElasticSearchService.class);

  @Value("${service.elasticsearch.url}")
  private String url;

  @Autowired
  private RestTemplate restTemplate;


  public void uploadMatchingRuleEvent(Rule rule, CallRecordEntity entity) {
    PhoneWithRule object = new PhoneWithRule(System.currentTimeMillis(), entity.getCallingNumber(), rule);
    String json = JsonUtils.toJson(object);
    if (json != null) {
      ResponseEntity<String> response = restTemplate.postForEntity(url + "/" + object.hashCode(),
          json, String.class);
      if (response.getStatusCode().is2xxSuccessful()) {
        log.info("Document for phone='{}' uploaded", entity.getCallingNumber());
      }
    }
  }

  static class PhoneWithRule {

    @JsonProperty("@timestamp")
    long timestamp;
    String phoneNumber;
    Rule rule;

    public PhoneWithRule() {
    }

    public PhoneWithRule(long timestamp, String phoneNumber, Rule rule) {
      this.timestamp = timestamp;
      this.phoneNumber = phoneNumber;
      this.rule = rule;
    }

    public String getPhoneNumber() {
      return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }

    public Rule getRule() {
      return rule;
    }

    public void setRule(Rule rule) {
      this.rule = rule;
    }

    public long getTimestamp() {
      return timestamp;
    }

    public void setTimestamp(long timestamp) {
      this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      PhoneWithRule that = (PhoneWithRule) o;

      if (!phoneNumber.equals(that.phoneNumber)) {
        return false;
      }
      return rule.equals(that.rule);
    }

    @Override
    public int hashCode() {
      int result = phoneNumber.hashCode();
      result = 31 * result + rule.hashCode();
      return result;
    }
  }

}
