package hack2hire13.stream.app.services;

import hack2hire13.stream.app.domain.Rule;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Andrei Safronov
 */
@Service
public class RuleService {

  @Value("${service.rule.url}")
  private String url;

  @Autowired
  private RestTemplate restTemplate;

  private final CopyOnWriteArrayList<Rule> rules = new CopyOnWriteArrayList<>();

  public List<Rule> getRules() {
    if (rules.isEmpty()) {
      synchronized (this) {
        if (rules.isEmpty()) {
          ResponseEntity<List<Rule>> entity = restTemplate.exchange(
              url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Rule>>() {
              });
          if (entity.getStatusCode().is2xxSuccessful()) {
            rules.addAll(entity.getBody());
          }
        }
      }
    }

    return rules;
  }

}
