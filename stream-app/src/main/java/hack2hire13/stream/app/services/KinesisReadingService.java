package hack2hire13.stream.app.services;

import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.model.DescribeStreamRequest;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.model.GetRecordsRequest;
import com.amazonaws.services.kinesis.model.GetRecordsResult;
import com.amazonaws.services.kinesis.model.GetShardIteratorRequest;
import com.amazonaws.services.kinesis.model.Record;
import com.amazonaws.services.kinesis.model.Shard;
import hack2hire13.stream.app.domain.CDRType;
import hack2hire13.stream.app.domain.CallDateRecord;
import hack2hire13.stream.app.domain.CallRecordEntity;
import hack2hire13.stream.app.domain.CauseForTermination;
import hack2hire13.stream.app.domain.Rule;
import hack2hire13.stream.app.utils.JsonUtils;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Andrei Safronov
 */
@Service
public class KinesisReadingService {

  private static final Logger log = LoggerFactory.getLogger(KinesisReadingService.class);

  @Autowired
  private AmazonKinesis amazonKinesisClient;

  @Autowired
  private CallRecordService callRecordService;

  @Autowired
  private RuleMatchingService ruleMatchingService;

  @Autowired
  private ElasticSearchService elasticSearchService;

  @Value("${service.kinesis.stream.name}")
  private String streamName;

  @Value("${service.kinesis.threads}")
  private int threads;

  @Value("${service.kinesis.shard.iterator}")
  private String shardIteratorType;

  @Value("${service.kinesis.sleep.period.ms}")
  private long sleepPeriod;

  private ExecutorService streamHandlingExecutor;

  @PostConstruct
  void init() {
    streamHandlingExecutor = Executors.newFixedThreadPool(threads);

    List<Shard> shards = getKinesisShards();
    shards.forEach(shard -> {
      KinesisReadingWorker worker = new KinesisReadingService.KinesisReadingWorker(shard);
      streamHandlingExecutor.submit(worker);
    });
  }

  @PreDestroy
  void close() {
    streamHandlingExecutor.shutdown();
  }

  private List<Shard> getKinesisShards() {
    DescribeStreamRequest request = new DescribeStreamRequest();
    request.setStreamName(streamName);
    List<Shard> shards = new ArrayList<>();
    String exclusiveStartShardId = null;
    do {
      request.setExclusiveStartShardId(exclusiveStartShardId);
      DescribeStreamResult result = amazonKinesisClient.describeStream(request);
      shards.addAll(result.getStreamDescription().getShards());
      if (result.getStreamDescription().getHasMoreShards() && shards.size() > 0) {
        exclusiveStartShardId = shards.get(shards.size() - 1).getShardId();
      } else {
        exclusiveStartShardId = null;
      }
    } while (exclusiveStartShardId != null);

    log.info("Read {} shards from kinesis", shards.size());
    return shards;
  }

  private class KinesisReadingWorker implements Runnable {

    private Shard shard;

    KinesisReadingWorker(Shard shard) {
      this.shard = shard;
    }

    @Override
    public void run() {
      String shardIterator;
      GetShardIteratorRequest getShardIteratorRequest = new GetShardIteratorRequest();
      getShardIteratorRequest.setStreamName(streamName);
      getShardIteratorRequest.setShardId(shard.getShardId());
      getShardIteratorRequest.setShardIteratorType(shardIteratorType);

      shardIterator = amazonKinesisClient.getShardIterator(getShardIteratorRequest).getShardIterator();

      while (true) {
        GetRecordsRequest request = new GetRecordsRequest();
        request.setShardIterator(shardIterator);
        request.setLimit(5);

        GetRecordsResult result = amazonKinesisClient.getRecords(request);
        List<Record> records = result.getRecords();
        if (records != null && !records.isEmpty()) {
          for (Record record : records) {
            String content = new String(record.getData().array(), StandardCharsets.UTF_8);
            CallDateRecord callDateRecord = JsonUtils.fromJson(content, CallDateRecord.class);
            if (isRecordValid(callDateRecord)) {
              CallRecordEntity entity = callRecordService.persistCallRecord(callDateRecord);
              if (entity != null) {
                List<Rule> rules = ruleMatchingService.getMatchingRules(entity);
                if (rules != null && !rules.isEmpty()) {
                  rules.forEach(rule -> elasticSearchService.uploadMatchingRuleEvent(rule, entity));
                }
              }
            }
          }
        }

        shardIterator = result.getNextShardIterator();
        sleep(sleepPeriod);
      }
    }

    private boolean isRecordValid(CallDateRecord record) {
      if (record == null)
        return false;

      String cdrType = record.getCdrType();
      if (cdrType == null || CDRType.getByCode(cdrType) == null)
        return false;

      if (record.getCallingNumber() == null)
        return false;

      if (record.getCalledNumber() == null)
        return false;

      if (record.getLocation() == null)
        return false;

      if (record.getCallDuration() == null)
        return false;

      String cause = record.getCauseForTermination();
      if (cause == null || CauseForTermination.getByCode(cause) == null)
        return false;

      return true;
    }

    private void sleep(long millis) {
      try {
        Thread.sleep(millis);
      }
      catch (InterruptedException exception) {
        throw new RuntimeException(exception);
      }
    }
  }

}
