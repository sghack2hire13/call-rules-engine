package hack2hire13.stream.app.services;

import static hack2hire13.stream.app.domain.CauseForTermination.*;

import hack2hire13.stream.app.dao.CallRecordEntityRepository;
import hack2hire13.stream.app.domain.AggregationType;
import hack2hire13.stream.app.domain.CDRType;
import hack2hire13.stream.app.domain.CallDateRecord;
import hack2hire13.stream.app.domain.CallRecordEntity;
import hack2hire13.stream.app.domain.CauseForTermination;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Andrei Safronov
 */
@Service
public class CallRecordService {

  private static final Logger log = LoggerFactory.getLogger(CallRecordService.class);

  private static final ThreadLocal<SimpleDateFormat> DATE_FORMAT = ThreadLocal
      .withInitial(() -> new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));

  @Autowired
  private CallRecordEntityRepository repository;

  public CallRecordEntity persistCallRecord(CallDateRecord record) {
    CallRecordEntity entity = buildEntity(record);
    if (entity != null) {
      return persist(entity);
    } else {
      return null;
    }
  }

  @Transactional
  private CallRecordEntity persist(CallRecordEntity entity) {
    try {
      return repository.save(entity);
    } catch (Exception e) {
      log.error("Cannot save entity", e);
      return null;
    }
  }

  public Double getCallDuration(CallRecordEntity record, AggregationType aggregation) {
    switch (aggregation) {
      case DAILY:
        return repository.findCallDurationDailyAggregation(record.getCallingNumber(), record.getDay(), NORMAL_CALL);
      case WEEKLY:
        return repository.findCallDurationWeeklyAggregation(record.getCallingNumber(), record.getWeek(), NORMAL_CALL);
      case MONTHLY:
        return repository.findCallDurationMonthlyAggregation(record.getCallingNumber(), record.getMonth(), NORMAL_CALL);
      case YEARLY:
        return repository.findCallDurationYearlyAggregation(record.getCallingNumber(), record.getYear(), NORMAL_CALL);
      default:
        throw new IllegalArgumentException("Unknown value: " + aggregation);
    }
  }

  public Long getNumberOfCalls(CallRecordEntity record, AggregationType aggregation) {
    switch (aggregation) {
      case DAILY:
        return repository.findNumberOfCallsDailyAggregation(record.getCallingNumber(), record.getCauseForTermination(),
            record.getDay());
      case WEEKLY:
        return repository.findNumberOfCallsWeeklyAggregation(record.getCallingNumber(), record.getCauseForTermination(),
            record.getWeek());
      case MONTHLY:
        return repository.findNumberOfCallsMonthlyAggregation(record.getCallingNumber(),
            record.getCauseForTermination(), record.getMonth());
      case YEARLY:
        return repository.findNumberOfCallsYearlyAggregation(record.getCallingNumber(), record.getCauseForTermination(),
            record.getYear());
      default:
        throw new IllegalArgumentException("Unknown value: " + aggregation);
    }
  }

  private CallRecordEntity buildEntity(CallDateRecord record) {
    try {
      CallRecordEntity entity = new CallRecordEntity();
      entity.setCdrType(CDRType.getByCode(record.getCdrType()));
      entity.setCallingNumber(record.getCallingNumber());
      entity.setCalledNumber(record.getCalledNumber());
      entity.setLocation(record.getLocation());
      entity.setCallDuration(record.getCallDuration());
      entity.setCauseForTermination(CauseForTermination.getByCode(record.getCauseForTermination()));

      Date parsed = DATE_FORMAT.get().parse(record.getAnswerTime());
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(parsed);

      entity.setDay(calendar.get(Calendar.DAY_OF_MONTH));
      entity.setWeek(calendar.get(Calendar.WEEK_OF_YEAR));
      entity.setMonth(calendar.get(Calendar.MONTH) + 1);
      entity.setYear(calendar.get(Calendar.YEAR));

      return entity;
    } catch (Exception e) {
      log.error("Error while building CallRecordEntity", e);
      return null;
    }
  }

}
