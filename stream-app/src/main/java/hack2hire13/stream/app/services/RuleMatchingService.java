package hack2hire13.stream.app.services;

import static hack2hire13.stream.app.domain.CauseForTermination.*;
import static hack2hire13.stream.app.domain.EventType.*;

import hack2hire13.stream.app.domain.AggregationType;
import hack2hire13.stream.app.domain.CallRecordEntity;
import hack2hire13.stream.app.domain.CauseForTermination;
import hack2hire13.stream.app.domain.EventType;
import hack2hire13.stream.app.domain.Rule;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Andrei Safronov
 */
@Service
public class RuleMatchingService {

  @Autowired
  private RuleService ruleService;

  @Autowired
  private CallRecordService callRecordService;

  public List<Rule> getMatchingRules(CallRecordEntity record) {
    List<Rule> rules = findPossibleRulesForRecord(record);
    List<Rule> matchingRules = new ArrayList<>();

    for (Rule rule : rules) {
      AggregationType aggregation = rule.getAggregationType();
      EventType eventType = rule.getEventType();
      Double value;
      if (eventType == ALL_CALLS_DURATION) {
        value = callRecordService.getCallDuration(record, aggregation);
      } else {
        value = callRecordService.getNumberOfCalls(record, aggregation).doubleValue();
      }

      if (isRuleSatisfied(rule, value)) {
        matchingRules.add(rule);
      }
    }

    return matchingRules;
  }

  private List<Rule> findPossibleRulesForRecord(CallRecordEntity record) {
    List<Rule> rules = ruleService.getRules();
    CauseForTermination cause = record.getCauseForTermination();
    List<Rule> matchingRules = new ArrayList<>();
    for (Rule rule : rules) {
      if (cause == NORMAL_CALL && isNormalCallRule(rule)) {
        matchingRules.add(rule);
      } else if (cause == ABNORMALLY_DROPPED && isAbnormallyDroppedCallRule(rule)) {
        matchingRules.add(rule);
      } else if (cause == UNSUCCESSFUL_ATTEMPT && isUnsuccessfulAttemptRule(rule)) {
        matchingRules.add(rule);
      }
    }

    return matchingRules;
  }

  private static boolean isNormalCallRule(Rule rule) {
    EventType event = rule.getEventType();
    return event == ALL_CALLS_DURATION || event == NO_OF_CALLS;
  }

  private static boolean isAbnormallyDroppedCallRule(Rule rule) {
    EventType event = rule.getEventType();
    return event == ABNORMALLY_DROPPED_CALLS;
  }

  private static boolean isUnsuccessfulAttemptRule(Rule rule) {
    EventType event = rule.getEventType();
    return event == UNSUCCESSFUL_ATTEMPTS;
  }

  private static boolean isRuleSatisfied(Rule rule, Double value) {
    Double ruleValue = rule.getParameter();
    switch (rule.getOperator()) {
      case EQUALS:
        return Math.abs(ruleValue - value) < 1e-6;
      case GREATER:
        return value > ruleValue;
      case GREATER_OR_EQUALS:
        return value > ruleValue || Math.abs(ruleValue - value) < 1e-6;
      case LESS:
        return value < ruleValue;
      case LESS_OR_EQUALS:
        return value < ruleValue || Math.abs(ruleValue - value) < 1e-6;
      default:
        throw new IllegalArgumentException("Unknown value: " + rule);
    }
  }
}
