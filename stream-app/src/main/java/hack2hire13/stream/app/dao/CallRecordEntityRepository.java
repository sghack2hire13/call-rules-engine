package hack2hire13.stream.app.dao;

import hack2hire13.stream.app.domain.CallRecordEntity;
import hack2hire13.stream.app.domain.CauseForTermination;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Andrei Safronov
 */
@Repository
public interface CallRecordEntityRepository extends PagingAndSortingRepository<CallRecordEntity, Long> {

  @Query("SELECT SUM(c.callDuration) FROM CallRecordEntity c where c.callingNumber = :number and c.day = :day and c.causeForTermination = :causeForTermination")
  Double findCallDurationDailyAggregation(@Param("number") String callingNumber, @Param("day") int day,
      @Param("causeForTermination") CauseForTermination causeForTermination);

  @Query("SELECT SUM(c.callDuration) FROM CallRecordEntity c where c.callingNumber = :number and c.week = :week and c.causeForTermination = :causeForTermination")
  Double findCallDurationWeeklyAggregation(@Param("number") String callingNumber, @Param("week") int week,
      @Param("causeForTermination") CauseForTermination causeForTermination);

  @Query("SELECT SUM(c.callDuration) FROM CallRecordEntity c where c.callingNumber = :number and c.month = :month and c.causeForTermination = :causeForTermination")
  Double findCallDurationMonthlyAggregation(@Param("number") String callingNumber, @Param("month") int month,
      @Param("causeForTermination") CauseForTermination causeForTermination);

  @Query("SELECT SUM(c.callDuration) FROM CallRecordEntity c where c.callingNumber = :number and c.year = :year and c.causeForTermination = :causeForTermination")
  Double findCallDurationYearlyAggregation(@Param("number") String callingNumber, @Param("year") int year,
      @Param("causeForTermination") CauseForTermination causeForTermination);


  @Query("SELECT COUNT(*) FROM CallRecordEntity c where c.callingNumber = :number and c.day = :day and c.causeForTermination = :causeForTermination")
  Long findNumberOfCallsDailyAggregation(@Param("number") String callingNumber,
      @Param("causeForTermination") CauseForTermination cause, @Param("day") int day);

  @Query("SELECT COUNT(*) FROM CallRecordEntity c where c.callingNumber = :number and c.week = :week and c.causeForTermination = :causeForTermination")
  Long findNumberOfCallsWeeklyAggregation(@Param("number") String callingNumber,
      @Param("causeForTermination") CauseForTermination cause, @Param("week") int week);

  @Query("SELECT COUNT(*) FROM CallRecordEntity c where c.callingNumber = :number and c.month = :month and c.causeForTermination = :causeForTermination")
  Long findNumberOfCallsMonthlyAggregation(@Param("number") String callingNumber,
      @Param("causeForTermination") CauseForTermination cause, @Param("month") int month);

  @Query("SELECT COUNT(*) FROM CallRecordEntity c where c.callingNumber = :number and c.year = :year and c.causeForTermination = :causeForTermination")
  Long findNumberOfCallsYearlyAggregation(@Param("number") String callingNumber,
      @Param("causeForTermination") CauseForTermination cause, @Param("year") int year);
}
