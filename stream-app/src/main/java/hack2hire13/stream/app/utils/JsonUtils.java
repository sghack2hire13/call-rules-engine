package hack2hire13.stream.app.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andrei Safronov
 */
public final class JsonUtils {

  private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);
  private static final ObjectMapper MAPPER = new ObjectMapper();

  private JsonUtils() {
  }

  public static <T> T fromJson(String json, Class<T> clazz) {
    try {
      return MAPPER.readValue(json, clazz);
    } catch (Exception e) {
      log.error("Cannot deserialize json", e);
      return null;
    }
  }

  public static String toJson(Object object) {
    try {
      return MAPPER.writeValueAsString(object);
    } catch (Exception e) {
      log.error("Cannot serialize json", e);
      return null;
    }
  }

}
