package hack2hire13.stream.app;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author Andrei Safronov
 */
@SpringBootApplication
public class App extends SpringBootServletInitializer {

  @Value("${service.aws.accesskey}")
  private String accessKey;

  @Value("${service.aws.secret}")
  private String secret;

  @Value("${service.aws.region}")
  private String region;

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder.build();
  }

  @Bean
  public AWSCredentials awsCredentials() {
    return new BasicAWSCredentials(accessKey, secret);
  }

  @Bean
  public Region awsRegion() {
    return RegionUtils.getRegion(region);
  }

  @Bean
  public AmazonKinesis amazonKinesisClient(AWSCredentials awsCredentials, Region awsRegion) {
    AmazonKinesis client = new AmazonKinesisClient(awsCredentials);
    client.setRegion(awsRegion);
    return client;
  }


  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

}
